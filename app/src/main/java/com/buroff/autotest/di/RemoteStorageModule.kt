package com.buroff.autotest.di

import com.buroff.autotest.GlobalSettings
import com.buroff.autotest.data.retrofit.CarApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import javax.xml.datatype.DatatypeConstants.SECONDS
import android.net.sip.SipErrorCode.TIME_OUT
import com.buroff.autotest.data.storage.CarDataRemoteStorage
import com.buroff.autotest.data.storage.CarDataRemoteStorageImpl
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit


@Module
class RemoteStorageModule {

    @Provides
    fun provideCarDataRemoteStorage(service: CarApi): CarDataRemoteStorage =
        CarDataRemoteStorageImpl(service, Schedulers.io(), GlobalSettings.API_KEY, GlobalSettings.PAGE_SIZE)
}
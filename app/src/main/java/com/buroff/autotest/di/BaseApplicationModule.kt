package com.buroff.autotest.di

import com.buroff.autotest.ui.view.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BaseApplicationModule {
    @ContributesAndroidInjector
    abstract fun contributeActivityInjector(): MainActivity
}

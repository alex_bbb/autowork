package com.buroff.autotest.di

import com.buroff.autotest.data.storage.CarDataRemoteStorage
import com.buroff.autotest.ui.navigator.AppRouter
import com.buroff.autotest.ui.presenter.BuildDatesPresenter
import dagger.Module
import dagger.Provides

@Module
class BuildDatesFragmentScreenModule {

    @Provides
    fun provideBuildDatesPresenter(
        router: AppRouter,
        carDataRemoteStorage: CarDataRemoteStorage
    ): BuildDatesPresenter = BuildDatesPresenter(router, carDataRemoteStorage)
}

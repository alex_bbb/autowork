package com.buroff.autotest.di

import com.buroff.autotest.GlobalSettings
import com.buroff.autotest.data.retrofit.CarApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import android.util.Log
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import java.io.IOException
import java.util.concurrent.TimeUnit


@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideCarRetrofitApi(retrofit: Retrofit): CarApi = retrofit.create(CarApi::class.java)

    @Provides
    @Singleton
    fun provideCarRetrofitBuilder(client: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(GlobalSettings.SERVER)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()

    @Provides
    @Singleton
    fun provideHttpClient(): OkHttpClient {
        val httpBuilder = OkHttpClient.Builder()
        httpBuilder.connectTimeout(5, TimeUnit.SECONDS)
        httpBuilder.readTimeout(10, TimeUnit.SECONDS)
        httpBuilder.addInterceptor(LoggingInterceptor())
        httpBuilder.retryOnConnectionFailure(false)
        return httpBuilder.build()
    }

    internal inner class LoggingInterceptor : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val request = chain.request()

            val t1 = System.nanoTime()
            Log.d(
                "AutoOkHttp", String.format(
                    "Sending request %s on %s%n%s",
                    request.url(), chain.connection(), request.headers()
                )
            )

            val response = chain.proceed(request)

            val t2 = System.nanoTime()
            Log.d(
                "AutoOkHttp", String.format(
                    "Received response for %s in %.1fms%n%s",
                    response.request().url(), (t2 - t1) / 1e6, response.headers()
                )
            )
            return response
        }
    }
}
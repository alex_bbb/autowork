package com.buroff.autotest.di

import com.buroff.autotest.ui.navigator.AppRouter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import javax.inject.Singleton

@Module
class CiceroneModule {

    private val cicerone: Cicerone<AppRouter> = Cicerone.create(AppRouter())

    @Provides
    @Singleton
    fun provideCicerone(): Cicerone<AppRouter> = cicerone

    @Provides
    @Singleton
    fun router(): AppRouter = cicerone.router

    @Provides
    @Singleton
    fun navigatorHolder(): NavigatorHolder = cicerone.navigatorHolder
}
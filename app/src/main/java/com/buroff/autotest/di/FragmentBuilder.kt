package com.buroff.autotest.di

import com.buroff.autotest.ui.view.BuildDatesFragment
import com.buroff.autotest.ui.view.MainActivity
import com.buroff.autotest.ui.view.ManufacturerFragment
import com.buroff.autotest.ui.view.ModelFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class FragmentBuilder {

    @ContributesAndroidInjector(modules = [ModelFragmentScreenModule::class])
    internal abstract fun bindModelFragment(): ModelFragment

    @ContributesAndroidInjector(modules = [ManufacturerFragmentScreenModule::class])
    internal abstract fun bindManufacturerFragment(): ManufacturerFragment

    @ContributesAndroidInjector(modules = [BuildDatesFragmentScreenModule::class])
    internal abstract fun bindBuildDatesFragment(): BuildDatesFragment

}
package com.buroff.autotest.di

import com.buroff.autotest.data.storage.CarDataRemoteStorage
import com.buroff.autotest.ui.navigator.AppRouter
import com.buroff.autotest.ui.presenter.ModelPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ModelFragmentScreenModule {

    @Provides
    fun provideModelPresenter(
        router: AppRouter,
        carDataRemoteStorage: CarDataRemoteStorage
    ): ModelPresenter = ModelPresenter(router, carDataRemoteStorage)
}

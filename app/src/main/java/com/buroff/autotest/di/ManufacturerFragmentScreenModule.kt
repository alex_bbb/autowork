package com.buroff.autotest.di

import com.buroff.autotest.data.storage.CarDataRemoteStorage
import com.buroff.autotest.ui.navigator.AppRouter
import com.buroff.autotest.ui.presenter.ManufacturerPresenter
import com.buroff.autotest.ui.presenter.ModelPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ManufacturerFragmentScreenModule {

    @Provides
    fun provideManufacturerPresenter(
        router: AppRouter,
        carDataRemoteStorage: CarDataRemoteStorage
    ): ManufacturerPresenter = ManufacturerPresenter(router, carDataRemoteStorage)
}

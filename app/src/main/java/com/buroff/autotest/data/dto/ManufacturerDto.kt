package com.buroff.autotest.data.dto

data class ManufacturerDto(
    val page: Int,
    val pageSize: Int,
    val totalPageCount: Int,
    val wkda: Map<String, String>
    )

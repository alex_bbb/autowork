package com.buroff.autotest.data.storage

import com.buroff.autotest.data.dto.BuildDatesDto
import com.buroff.autotest.data.dto.MainTypesDto
import com.buroff.autotest.data.dto.ManufacturerDto
import io.reactivex.Single

interface CarDataRemoteStorage {
    fun loadByManufacturer(manufacturer: String, page: Int): Single<MainTypesDto>
    fun loadByBuildDates(manufacturer: String, model: String, page: Int): Single<BuildDatesDto>
    fun loadAll(page: Int): Single<ManufacturerDto>
}
package com.buroff.autotest.data.models

data class TextItem(val textkey: String, val text: String)
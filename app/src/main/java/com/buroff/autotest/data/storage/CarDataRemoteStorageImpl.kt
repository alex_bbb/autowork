package com.buroff.autotest.data.storage

import com.buroff.autotest.data.dto.BuildDatesDto
import com.buroff.autotest.data.dto.MainTypesDto
import com.buroff.autotest.data.dto.ManufacturerDto
import com.buroff.autotest.data.retrofit.CarApi
import io.reactivex.Scheduler
import io.reactivex.Single

class CarDataRemoteStorageImpl(
    private val api: CarApi,
    private val scheduler: Scheduler,
    private val waKey: String,
    private val pageSize: Int
) : CarDataRemoteStorage {

    override fun loadByManufacturer(manufacturer: String, page: Int): Single<MainTypesDto> {
        return api.loadByManufacturer(manufacturer, page, pageSize, waKey)
            .subscribeOn(scheduler)
    }

    override fun loadByBuildDates(manufacturer: String, model: String, page: Int): Single<BuildDatesDto> {
        return api.loadByBuildDates(manufacturer, model, waKey)
            .subscribeOn(scheduler)
    }

    override fun loadAll(page: Int): Single<ManufacturerDto> {
        return api.loadAll(page, pageSize, waKey)
            .subscribeOn(scheduler)
    }
}
package com.buroff.autotest.data.retrofit

import com.buroff.autotest.data.dto.BuildDatesDto
import com.buroff.autotest.data.dto.MainTypesDto
import com.buroff.autotest.data.dto.ManufacturerDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CarApi {
    @GET("/v1/car-types/main-types")
    fun loadByManufacturer(
        @Query("manufacturer") manufacturer: String,
        @Query("page") page: Int,
        @Query("pageSize") pageSize: Int,
        @Query("wa_key") waKey: String
    ): Single<MainTypesDto>

    @GET("/v1/car-types/manufacturer")
    fun loadAll(
        @Query("page") page: Int,
        @Query("pageSize") pageSize: Int,
        @Query("wa_key") waKey: String
    ): Single<ManufacturerDto>

    @GET("v1/car-types/built-dates")
    fun loadByBuildDates(
        @Query("manufacturer") manufacturer: String,
        @Query("main-type") mainType: String,
        @Query("wa_key") waKey: String
    ): Single<BuildDatesDto>

}
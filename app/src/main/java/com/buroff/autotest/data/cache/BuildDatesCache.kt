package com.buroff.autotest.data.cache

import com.buroff.autotest.data.models.TextItem

object buildDatesCache {

    val data: MutableList<TextItem> = mutableListOf()
    var maxPages: Int = 0
    var currentPage: Int = 0

    fun clear() {
        data.clear()
        maxPages = 0
        currentPage = 0
    }

}
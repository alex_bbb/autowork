package com.buroff.autotest.data.dto

data class BuildDatesDto(
    val page: Int,
    val pageSize: Int = 0,
    val totalPageCount: Int = 0,
    val wkda: Map<String, String>
    )

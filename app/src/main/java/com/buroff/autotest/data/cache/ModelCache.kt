package com.buroff.autotest.data.cache

import com.buroff.autotest.data.models.TextItem

object modelCache {

    val data: MutableList<TextItem> = mutableListOf()
    var maxPages: Int = 0
    var currentPage: Int = 0

}
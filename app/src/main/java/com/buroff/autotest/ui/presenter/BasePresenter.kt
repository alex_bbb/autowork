package com.buroff.autotest.ui.presenter

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.buroff.autotest.data.models.TextItem
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenter<T : MvpView> : MvpPresenter<T>() {

    protected lateinit var argumentData: String

    fun withArg(model: String) {
        this.argumentData = model
    }

    protected val compositeDisposable = CompositeDisposable()

    fun Disposable.disposeOnDestroy(): Disposable {
        compositeDisposable.add(this)
        return this
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    abstract fun getDataset(): MutableList<TextItem>

    abstract fun loadNextPage()

    abstract fun onItemClick(item: TextItem)
}
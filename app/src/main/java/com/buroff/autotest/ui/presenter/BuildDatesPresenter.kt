package com.buroff.autotest.ui.presenter

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.buroff.autotest.data.cache.buildDatesCache
import com.buroff.autotest.data.cache.manufacturerCache
import com.buroff.autotest.data.cache.modelCache
import com.buroff.autotest.data.models.TextItem
import com.buroff.autotest.data.storage.CarDataRemoteStorage
import com.buroff.autotest.ui.navigator.AppRouter
import com.buroff.autotest.ui.navigator.Screens
import com.buroff.autotest.ui.view.BaseView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo

@InjectViewState
class BuildDatesPresenter(
    val router: AppRouter,
    val carDataRemoteStorage: CarDataRemoteStorage
) : BasePresenter<BaseView>() {

    protected lateinit var manufacturer: String
    protected lateinit var model: String

    fun withArg(manufacturer: String, model: String) {
        this.manufacturer = manufacturer
        this.model = model
    }
    //Using as a cache
    val cache = buildDatesCache

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        if(cache.currentPage == 0) {
            carDataRemoteStorage.loadByBuildDates(manufacturer, model, 0)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { dto ->
                        dto.wkda.map { entry ->
                            cache.data.add(TextItem(entry.key, entry.value))
                        }
                        cache.maxPages = dto.totalPageCount
                        cache.currentPage = 1;

                        viewState.notifyDataSetChanged()
                    }, {
                        it.printStackTrace()
                    })
                .addTo(compositeDisposable)
        }
    }

    override fun getDataset(): MutableList<TextItem> {
        return cache.data;
    }

    override fun loadNextPage() {
        if(cache.currentPage < cache.maxPages) {
            carDataRemoteStorage.loadByBuildDates(manufacturer, model, cache.currentPage)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { dto ->
                        dto.wkda.map { entry ->
                            cache.data.add(TextItem(entry.key, entry.value))
                        }
                        cache.maxPages = dto.totalPageCount
                        cache.currentPage++;

                        viewState.notifyDataSetChanged()
                    }, {
                        it.printStackTrace()
                    })
                .addTo(compositeDisposable)
        }
    }

    override fun onItemClick(item: TextItem) {
       //Do nothing
        Log.d("TTT", "clicked type {${item.text})")
    }
}
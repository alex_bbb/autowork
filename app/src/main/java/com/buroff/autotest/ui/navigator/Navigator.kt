package com.buroff.autotest.ui.navigator

import android.app.Activity
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import com.buroff.autotest.di.BuildDatesFragmentScreenModule
import com.buroff.autotest.ui.view.BuildDatesFragment
import com.buroff.autotest.ui.view.ManufacturerFragment
import com.buroff.autotest.ui.view.ModelFragment
import ru.terrakok.cicerone.android.SupportAppNavigator

class Navigator(val activity: FragmentActivity, holder: Int) : SupportAppNavigator(activity, holder) {

    override fun createActivityIntent(screenKey: String?, data: Any?): Intent? = null

    override fun createFragment(screenKey: String?, data: Any?): Fragment? = when (screenKey) {
        Screens.MODEL_SCREEN -> ModelFragment.newInstance(data as ModelFragment.Arguments)
        Screens.MANUFACTURER_SCREEN -> ManufacturerFragment.newInstance(data as ManufacturerFragment.Arguments)
        Screens.BUILD_DATES_SCREEN -> BuildDatesFragment.newInstance(data as BuildDatesFragment.Arguments)
        else -> null
    }
}
package com.buroff.autotest.ui.navigator

class Screens {
    companion object {
        val MODEL_SCREEN = "MODEL_SCREEN"
        val MANUFACTURER_SCREEN = "MANUFACTURER_SCREEN"
        val BUILD_DATES_SCREEN = "BUILD_DATES_SCREEN"
    }
}
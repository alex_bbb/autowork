package com.buroff.autotest.ui.view

import com.arellomobile.mvp.MvpView

interface BaseView: MvpView {
    fun notifyDataSetChanged()
}
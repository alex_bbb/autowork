package com.buroff.autotest.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.buroff.autotest.data.models.TextItem
import com.buroff.autotest.R



class TextItemRVAdapter(private val myDataset: MutableList<TextItem>, private val listener: OnItemClickListener) :
    RecyclerView.Adapter<TextItemRVAdapter.MyViewHolder>() {

    class MyViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view) {
        val textView: TextView = view.findViewById(R.id.text_item)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TextItemRVAdapter.MyViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false) as RelativeLayout
        layout.setOnClickListener { v ->
            listener.onItemClick(v.findViewById<TextView>(R.id.text_item).tag as TextItem)
         }
        return MyViewHolder(layout)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.textView.text = myDataset[position].text
        holder.textView.tag = myDataset[position]
    }

    override fun getItemCount() = myDataset.size

    interface OnItemClickListener {
        fun onItemClick(item: TextItem)
    }
}
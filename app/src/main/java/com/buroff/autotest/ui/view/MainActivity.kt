package com.buroff.autotest.ui.view

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.buroff.autotest.R
import com.buroff.autotest.ui.navigator.AppRouter
import com.buroff.autotest.ui.navigator.Navigator
import com.buroff.autotest.ui.navigator.Screens
import dagger.android.AndroidInjection
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.SupportAppNavigator
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    private val navigator = Navigator(this, R.id.fragment_holder)

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var router: AppRouter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigatorHolder.setNavigator(navigator)
        router.navigateTo(Screens.MODEL_SCREEN, ModelFragment.Arguments("testModel"))
    }
}

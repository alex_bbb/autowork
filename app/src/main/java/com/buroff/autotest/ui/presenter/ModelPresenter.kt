package com.buroff.autotest.ui.presenter

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.buroff.autotest.data.cache.buildDatesCache
import com.buroff.autotest.data.cache.manufacturerCache
import com.buroff.autotest.data.cache.modelCache
import com.buroff.autotest.data.models.TextItem
import com.buroff.autotest.data.storage.CarDataRemoteStorage
import com.buroff.autotest.ui.navigator.AppRouter
import com.buroff.autotest.ui.navigator.Screens
import com.buroff.autotest.ui.view.BaseView
import com.buroff.autotest.ui.view.ManufacturerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo

@InjectViewState
class ModelPresenter(
    val router: AppRouter,
    val carDataRemoteStorage: CarDataRemoteStorage
) : BasePresenter<BaseView>() {

    //Using as a cache
    val cache = modelCache

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        if(cache.currentPage == 0) {
            carDataRemoteStorage.loadAll(0)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { dto ->
                        dto.wkda.map { entry ->
                            cache.data.add(TextItem(entry.key, entry.value))
                        }
                        cache.maxPages = dto.totalPageCount
                        cache.currentPage = 1;

                        viewState.notifyDataSetChanged()
                    }, {
                        it.printStackTrace()
                    })
                .addTo(compositeDisposable)
        }
    }

    override fun getDataset(): MutableList<TextItem> {
        return cache.data;
    }

    override fun loadNextPage() {
        if(cache.currentPage < cache.maxPages) {
            carDataRemoteStorage.loadAll(cache.currentPage)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { dto ->
                        dto.wkda.map { entry ->
                            cache.data.add(TextItem(entry.key, entry.value))
                        }
                        cache.maxPages = dto.totalPageCount
                        cache.currentPage++;

                        viewState.notifyDataSetChanged()
                    }, {
                        it.printStackTrace()
                    })
                .addTo(compositeDisposable)
        }
    }

    override fun onItemClick(item: TextItem) {
        //Clear all subsequent caches
        manufacturerCache.clear()
        buildDatesCache.clear()
        router.navigateTo(Screens.MANUFACTURER_SCREEN, ManufacturerFragment.Arguments(item.textkey))
        Log.d("TTT", "clicked model {${item.text})")
    }
}
package com.buroff.autotest.ui.view


import android.os.Bundle
import android.os.Parcelable
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.buroff.autotest.data.models.TextItem
import com.buroff.autotest.ui.adapters.EndlessRecyclerViewScrollListener
import com.buroff.autotest.ui.adapters.TextItemRVAdapter
import com.buroff.autotest.ui.presenter.BuildDatesPresenter
import com.buroff.autotest.ui.presenter.ManufacturerPresenter
import com.buroff.autotest.ui.presenter.ModelPresenter
import com.buroff.autotest.util.args
import com.buroff.autotest.util.extraKey
import com.buroff.autotest.util.withArgs
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject
import javax.inject.Provider


class BuildDatesFragment : MvpAppCompatFragment(), BaseView {

    companion object {
        private val ARG by extraKey()
        private val RECYCLER_STATE by extraKey()

        fun newInstance(args: Arguments) = BuildDatesFragment().withArgs {
            putParcelable(ARG, args)
        }
    }

    @Parcelize
    data class Arguments(val manufacturer:String, val model:String) : Parcelable

    private val arg: Arguments by args(ARG)

    @Inject
    lateinit var presenterProvider: Provider<BuildDatesPresenter>

    @InjectPresenter(type = PresenterType.LOCAL)
    lateinit var presenter: BuildDatesPresenter

    @ProvidePresenter(type = PresenterType.LOCAL)
    fun providePresenter(): BuildDatesPresenter {
        return presenterProvider.get().apply { withArg(arg.manufacturer, arg.model) }
    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(com.buroff.autotest.R.layout.fragment_model, container, false)
        viewManager = LinearLayoutManager(this.context)
        viewAdapter = TextItemRVAdapter(presenter.getDataset(), object:TextItemRVAdapter.OnItemClickListener {
            override fun onItemClick(item: TextItem) {
                presenter.onItemClick(item);
            }
        })
        recyclerView = root.findViewById(com.buroff.autotest.R.id.recycler_view)
        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
        val linearLayoutManager = viewManager as LinearLayoutManager
        recyclerView.addOnScrollListener(object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.loadNextPage();
            }
        })

        return root
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        savedInstanceState?.let {
            val state = it.getParcelable(RECYCLER_STATE) as Parcelable
            recyclerView.layoutManager.onRestoreInstanceState(state)
        }
    }

    override fun onSaveInstanceState(state: Bundle) {
        val recyclerState = viewManager.onSaveInstanceState()
        state.putParcelable(RECYCLER_STATE, recyclerState)
        super.onSaveInstanceState(state)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun notifyDataSetChanged() {
        viewAdapter.notifyDataSetChanged()
    }
}
